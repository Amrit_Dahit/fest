/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.admin.serviceImpl;

import com.admin.constant.StatusConstants;
import com.admin.dao.AdminDao;
import com.admin.dao.BillingPeriodDao;
import com.admin.dao.StatusDao;
import com.admin.dto.BillingPeriodDto;
import com.admin.dto.CollegeDto;
import com.admin.mapper.BillingPeriodMapper;
import com.admin.service.BillingPeriodService;
import com.payrollSystem.entity.common.BillingPeriod;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author dell
 */
@Stateless
public class BillingPeriodServiceImpl  implements BillingPeriodService {
    
    @EJB
    private BillingPeriodDao billingPeriodDao;
    
    @EJB
    private StatusDao statusDao;
    
    @EJB
    private AdminDao adminDao;
    
      @Override
    public boolean save(BillingPeriodDto billingPeriodDto) {
        return billingPeriodDao.save(convertToBillingPeriod(billingPeriodDto));
    }

    private BillingPeriod convertToBillingPeriod(BillingPeriodDto billingPeriodDto) {
        BillingPeriod billingPeriod = new BillingPeriod();
        billingPeriod.setCreatedByAdmin(adminDao.getById(billingPeriodDto.getCreatedByAdminDto().getId()));
        billingPeriod.setCreatedDate(new Date());
        billingPeriod.setName(billingPeriodDto.getName());
        billingPeriod.setCode(billingPeriodDto.getCode());
        billingPeriod.setDescription(billingPeriodDto.getDescription());
        billingPeriod.setHierarchy(billingPeriodDto.getHierarchy());
        billingPeriod.setActive(true);
        billingPeriod.setStatus(statusDao.getByDesc(StatusConstants.CREATE_APPROVE.getName()));
        return billingPeriod;
    }
    
    private void setCreateEditCommonParameters(BillingPeriod billingPeriod, BillingPeriodDto billingPeriodDto) {
        billingPeriod.setDescription(billingPeriodDto.getDescription());
        billingPeriod.setName(billingPeriodDto.getName());
        billingPeriod.setCode(billingPeriodDto.getCode());
        billingPeriod.setHierarchy(billingPeriodDto.getHierarchy());
        billingPeriod.setActive(true);
    }
    
    @Override
    public boolean delete(BillingPeriodDto billingPeriodDto) {
        BillingPeriod billingPeriod = billingPeriodDao.getById(billingPeriodDto.getId());
        billingPeriod.setDeletedDate(new Date());
        billingPeriod.setDeletedReason(billingPeriodDto.getDeletedReason());
        billingPeriod.setDeletedByAdmin(adminDao.getById(billingPeriodDto.getDeletedByAdminDto().getId()));
        billingPeriod.setStatus(statusDao.getByDesc(StatusConstants.DELETED_APPROVE.getName()));
        return billingPeriodDao.modify(billingPeriod);
    }
    
      @Override
    public boolean update(BillingPeriodDto billingPeriodDto) {
        BillingPeriod billingPeriod = billingPeriodDao.getById(billingPeriodDto.getId());
        billingPeriod.setLastUpdatedDate(new Date());
        billingPeriod.setUpdatedByAdmin(adminDao.getById(billingPeriodDto.getUpdatedByAdminDto().getId()));
        billingPeriod.setStatus(statusDao.getByDesc(StatusConstants.EDIT_APPROVE.getName()));
        setCreateEditCommonParameters(billingPeriod, billingPeriodDto);
        return billingPeriodDao.modify(billingPeriod);
    }
    
    @Override
    public boolean checkIfBillingPeriodNameAlreadyExists(BillingPeriodDto billingPeriodDto) {
        return billingPeriodDao.checkIfBillingPeriodNameAlreadyExists(billingPeriodDto);
    }

    @Override
    public boolean checkIfBillingPeriodCodeAlreadyExists(BillingPeriodDto billingPeriodDto) {
        return billingPeriodDao.checkIfBillingPeriodCodeAlreadyExists(billingPeriodDto);
    }

    @Override
    public List<BillingPeriodDto> findByCollegeId(CollegeDto collegeDto) {
        return BillingPeriodMapper.convertToDtos(billingPeriodDao.findAllByCollegeId(collegeDto));
    }

    @Override
    public List<BillingPeriodDto> findByCollegeIdForDropDown(CollegeDto collegeDto) {
        return BillingPeriodMapper.convertToDtosForDropDown(billingPeriodDao.findAllByCollegeId(collegeDto));
    }

}
