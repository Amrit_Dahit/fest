/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.admin.daoImpl;

import com.admin.constant.StatusConstants;
import com.admin.dao.BillingPeriodDao;
import com.admin.dto.BillingPeriodDto;
import com.payrollSystem.entity.common.BillingPeriod;
import javax.ejb.Stateless;
import javax.persistence.Query;

/**
 *
 * @author dell
 */
@Stateless
public class BillingPeriodDaoImpl  extends StatusableDaoImpl<BillingPeriod> implements BillingPeriodDao{
    
     public BillingPeriodDaoImpl() {
        super(BillingPeriod.class);
    }
      @Override
    public boolean checkIfBillingPeriodNameAlreadyExists(BillingPeriodDto billingPeriodDto) {
        StringBuilder stringBuilder = new StringBuilder("SELECT count(s.id) FROM BillingPeriod s WHERE s.createdByAdmin.college.id=:collegeId AND s.name=:billingPeriodName AND s.status.statusDesc NOT IN (:deletedStatusList)");
        if (billingPeriodDto.getId() != null) {
            stringBuilder.append("and s.id<>:billingPeriodId");
        }
        Query query = getEntityManager().createQuery(stringBuilder.toString());
        query.setParameter("collegeId", billingPeriodDto.getCreatedByAdminDto().getCollegeDto().getId());
        query.setParameter("billingPeriodName", billingPeriodDto.getName());
        query.setParameter("deletedStatusList", StatusConstants.deleteStatusList());
        if (billingPeriodDto.getId() != null) {
            query.setParameter("billingPeriodId", billingPeriodDto.getId());
        }
        return (Long) query.getSingleResult() > 0;
    }

    @Override
    public boolean checkIfBillingPeriodCodeAlreadyExists(BillingPeriodDto billingPeriodDto) {
        StringBuilder stringBuilder = new StringBuilder("SELECT count(s.id) FROM BillingPeriod s WHERE s.createdByAdmin.college.id=:collegeId AND s.code=:billingPeriodCode AND s.status.statusDesc NOT IN (:deletedStatusList)");
        if (billingPeriodDto.getId() != null) {
            stringBuilder.append("and s.id<>:billingPeriodId");
        }
        Query query = getEntityManager().createQuery(stringBuilder.toString());
        query.setParameter("collegeId", billingPeriodDto.getCreatedByAdminDto().getCollegeDto().getId());
        query.setParameter("deletedStatusList", StatusConstants.deleteStatusList());
        query.setParameter("billingPeriodCode", billingPeriodDto.getCode());
        if (billingPeriodDto.getId() != null) {
            query.setParameter("billingPeriodId", billingPeriodDto.getId());
        }
        return (Long) query.getSingleResult() > 0;
    }
    
    
}
