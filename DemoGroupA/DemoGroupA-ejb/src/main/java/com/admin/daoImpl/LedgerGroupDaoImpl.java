/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.admin.daoImpl;

import com.admin.constant.StatusConstants;
import com.admin.dao.LedgerGroupDao;
import com.admin.dto.LedgerGroupDto;
import com.payrollSystem.entity.common.LedgerGroup;
import javax.ejb.Stateless;
import javax.persistence.Query;

/**
 *
 * @author dell
 */
@Stateless
public class LedgerGroupDaoImpl extends StatusableDaoImpl<LedgerGroup> implements LedgerGroupDao{
    
    public LedgerGroupDaoImpl(){
        super(LedgerGroup.class);
    
    }
    
    @Override
    public  boolean checkIfLedgerGroupNameAlreadyExists(LedgerGroupDto ledgerGroupDto){
        StringBuilder stringBuilder = new StringBuilder("SELECT count(s.id) FROM LedgerGroup s WHERE s.createdByAdmin.college.id=:collegeId AND s.name=:ledgerGroupName AND s.status.statusDesc NOT IN (:deletedStatusList)");
        if (ledgerGroupDto.getId() != null) {
            stringBuilder.append("and s.id<>:ledgerGroupId");
        }
        Query query = getEntityManager().createQuery(stringBuilder.toString());
        query.setParameter("collegeId", ledgerGroupDto.getCreatedByAdminDto().getCollegeDto().getId());
        query.setParameter("ledgerGroupName", ledgerGroupDto.getName());
        query.setParameter("deletedStatusList", StatusConstants.deleteStatusList());
        if (ledgerGroupDto.getId() != null) {
            query.setParameter("ledgerGroupId", ledgerGroupDto.getId());
        }
        return (Long) query.getSingleResult() > 0;
    
    }
    
     @Override
    public boolean checkIfLedgerGroupCodeALreadyExists(LedgerGroupDto ledgerGroupDto){
        StringBuilder stringBuilder = new StringBuilder("SELECT count(s.id) FROM LedgerGroup s WHERE s.createdByAdmin.college.id=:collegeId AND s.code=:ledgerGroupCode AND s.status.statusDesc NOT IN (:deletedStatusList)");
        if (ledgerGroupDto.getId() != null) {
            stringBuilder.append("and s.id<>:ledgerGroupId");
        }
        Query query = getEntityManager().createQuery(stringBuilder.toString());
        query.setParameter("collegeId", ledgerGroupDto.getCreatedByAdminDto().getCollegeDto().getId());
        query.setParameter("deletedStatusList", StatusConstants.deleteStatusList());
        query.setParameter("ledgerGroupCode", ledgerGroupDto.getCode());
        if (ledgerGroupDto.getId() != null) {
            query.setParameter("ledgerGroupId", ledgerGroupDto.getId());
        }
        return (Long) query.getSingleResult() > 0;
    
    }
    
    
    
}
