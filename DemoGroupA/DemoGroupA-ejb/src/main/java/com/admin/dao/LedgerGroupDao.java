/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.admin.dao;

import com.admin.dto.LedgerGroupDto;
import com.payrollSystem.entity.common.LedgerGroup;
import javax.ejb.Local;

/**
 *
 * @author dell
 */
@Local
public interface LedgerGroupDao extends StatusableDao<LedgerGroup>{
    boolean checkIfLedgerGroupNameAlreadyExists(LedgerGroupDto ledgerGroupDto);
    
    boolean checkIfLedgerGroupCodeALreadyExists(LedgerGroupDto ledgerGroupDto);
}
