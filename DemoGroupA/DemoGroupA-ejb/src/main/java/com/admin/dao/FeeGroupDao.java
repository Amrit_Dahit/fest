/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.admin.dao;

import com.payrollSystem.entity.common.FeeGroup;
import javax.ejb.Local;
import com.admin.dto.FeeGroupDto;

/**
 *
 * @author dell
 */
@Local
public interface FeeGroupDao extends StatusableDao<FeeGroup>{
    boolean checkIfFeeGroupNameAlreadyExists(FeeGroupDto feeGroupDto);

    boolean checkIfFeeGroupCodeAlreadyExists(FeeGroupDto feeGroupDto);
    
}
