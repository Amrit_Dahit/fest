/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.admin.dao;

import com.admin.dto.BillingPeriodDto;
import com.payrollSystem.entity.common.BillingPeriod;
import javax.ejb.Local;

/**
 *
 * @author dell
 */
@Local
public interface BillingPeriodDao extends StatusableDao<BillingPeriod> {
    
    boolean checkIfBillingPeriodNameAlreadyExists(BillingPeriodDto billingPeriodDto);

    boolean checkIfBillingPeriodCodeAlreadyExists(BillingPeriodDto billingPeriodDto);
}
