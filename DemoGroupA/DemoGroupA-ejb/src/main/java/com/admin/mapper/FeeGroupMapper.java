/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.admin.mapper;

import com.admin.dto.FeeGroupDto;
import com.payrollSystem.entity.common.FeeGroup;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author dell
 */
public class FeeGroupMapper extends AbstractCodeMapper {
    
     public static FeeGroupDto convertToDto(FeeGroup feeGroup) {
        FeeGroupDto feeGroupDto = new FeeGroupDto();
          
        convertCommon(feeGroupDto, feeGroup);
        return feeGroupDto;
    }
     
     public static List<FeeGroupDto> convertToDtos(List<FeeGroup> feeGroups) {
        List<FeeGroupDto> feeGroupDtos = new ArrayList<>();
        for (FeeGroup feeGroup : feeGroups) {
            feeGroupDtos.add(convertToDto(feeGroup));
        }
        return feeGroupDtos;
    }
     
     public static FeeGroupDto convertToDtoForDropDown(FeeGroup feeGroup) {
        FeeGroupDto feeGroupDto = new FeeGroupDto();
        feeGroupDto.setCode(feeGroup.getCode());
        feeGroupDto.setId(feeGroup.getId());
        feeGroupDto.setName(feeGroup.getName());
        return feeGroupDto;
    }
     
      public static List<FeeGroupDto> convertToDtosForDropDown(List<FeeGroup> feeGroups) {
        List<FeeGroupDto> feeGroupDtos = new ArrayList<>();
        for (FeeGroup feeGroup : feeGroups) {
            feeGroupDtos.add(convertToDtoForDropDown(feeGroup));
        }
        return feeGroupDtos;
    }
    
}
